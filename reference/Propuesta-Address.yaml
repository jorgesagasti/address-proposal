openapi: 3.0.0
info:
  contact:
    email: it@openpass.com.ar
  title: Modificaciones en Address
  description: Módulo de Adresses de BHub Fintch API
  version: '1.0'
  license:
    name: Openpass©
    url: 'https://www.openpass.com.ar'
servers:
  - url: 'https://dev.openpass.com.ar/bhubApi/1.0.0'
paths:
  /address/reverse-external-geolocation:
    post:
      operationId: reverse-external-geolocation
      parameters:
        - in: header
          name: applicationId
          required: true
          schema:
            type: string
        - in: header
          name: deviceId
          required: true
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  ' ':
                    $ref: ../models/Address/Address.yaml
              examples:
                example:
                  value:
                    id: 306C3A449D0F0EFD216B
                    formatted: 'Rafaela 48,2300 Santa Fe Province, Argentina'
                    streetNumber: '48'
                    postalCode: '2300'
                    municipality:
                      id: 31880AFC4B1528A0AE2F
                      description: Castellanos Department
                      name: Castellanos Department
                      code: '294'
                    city:
                      id: 68B00000000000150931
                      description: Rafaela
                      name: Rafaela
                      code: '2095'
                    province:
                      id: 0013483030652DBE7F33
                      description: Santa Fe
                      name: Santa Fe
                      code: '121'
                    country:
                      id: 000B050C1DCD019B9C0E
                      description: Argentina
                      name: Argentina
                      code: ARG
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: Returns an Address from ExternalGeolocationData
      summary: Create an Address from Geolocation Data
      tags:
        - Address
      requestBody:
        content:
          application/json:
            schema:
              $ref: ../models/Address/ExternalGeolocationData.yaml
  /address/countries:
    get:
      operationId: get-adress-countries
      parameters:
        - in: header
          name: applicationId
          required: true
          schema:
            type: string
        - in: header
          name: deviceId
          required: true
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  '':
                    type: array
                    items:
                      $ref: ../models/Address/Country.yaml
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: Returns all available countries
      summary: Available Countries
      tags:
        - Country
  /address/country:
    post:
      operationId: post-address-country
      parameters:
        - in: header
          name: applicationId
          required: true
          schema:
            type: string
        - in: header
          name: deviceId
          required: true
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  '':
                    $ref: ../models/Address/Country.yaml
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: Adds a Country
      summary: Add a Country
      requestBody:
        content:
          application/json:
            schema:
              $ref: ../models/Address/Country.yaml
      tags:
        - Country
  '/address/country/{countryId}':
    get:
      operationId: get-address-country-countryId
      parameters:
        - in: header
          name: applicationId
          required: true
          schema:
            type: string
        - in: header
          name: deviceId
          required: true
          schema:
            type: string
        - description: Id of Country
          in: path
          name: countryId
          required: true
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: ../models/Address/Country.yaml
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: Returns a Country by Id
      tags:
        - Country
      summary: Get a Country by Id
    put:
      operationId: put-address-country-countryId
      parameters:
        - in: header
          name: applicationId
          required: true
          schema:
            type: string
        - in: header
          name: deviceId
          required: true
          schema:
            type: string
        - description: Id of Country
          in: path
          name: countryId
          required: true
          schema:
            type: string
      summary: Modify a Country by Id
      responses:
        '200':
          description: OK
      description: Modifies a Country by Id
      tags:
        - Country
      requestBody:
        content:
          application/json:
            schema:
              $ref: ../models/Address/Country.yaml
  '/address/country/{countryId}/provinces':
    get:
      operationId: get-adress-country-countryId-provinces
      parameters:
        - in: header
          name: applicationId
          required: true
          schema:
            type: string
        - in: header
          name: deviceId
          required: true
          schema:
            type: string
        - description: Id of Country
          in: path
          name: countryId
          required: true
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                items:
                  $ref: ../models/Address/Province.yaml
                type: array
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: Returns all availables Provinces from a Country
      summary: Available Provinces for Country
      tags:
        - Province
  '/address/country/{countryId}/province':
    post:
      operationId: post-address-country-countryId-provinces
      parameters:
        - in: header
          name: applicationId
          required: true
          schema:
            type: string
        - in: header
          name: deviceId
          required: true
          schema:
            type: string
        - description: Id of Country
          in: path
          name: countryId
          required: true
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                items:
                  $ref: ../models/Address/Province.yaml
                type: array
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: Adds a Province to a Country
      summary: Add a Province to a Country
      tags:
        - Province
  '/address/country/{countryId}/province/{provinceId}':
    get:
      operationId: get-adress-country-countryId-province-provinceId
      parameters:
        - in: header
          name: applicationId
          required: true
          schema:
            type: string
        - in: header
          name: deviceId
          required: true
          schema:
            type: string
        - description: Id of Country
          in: path
          name: countryId
          required: true
          schema:
            type: string
        - description: Id of Province
          in: path
          name: provinceId
          required: true
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: ../models/Address/Province.yaml
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: Get a Province by Id
      tags:
        - Province
      summary: Get a Province by Id
      requestBody:
        content:
          application/json:
            schema:
              $ref: ../models/Address/Province.yaml
    put:
      summary: Modify a Province by Id
      operationId: put-address-country-countryId-province-provinceId
      parameters:
        - in: header
          name: applicationId
          required: true
          schema:
            type: string
        - in: header
          name: deviceId
          required: true
          schema:
            type: string
        - description: Id of Country
          in: path
          name: countryId
          required: true
          schema:
            type: string
        - description: Id of Province
          in: path
          name: provinceId
          required: true
          schema:
            type: string
      responses:
        '200':
          description: OK
      description: Modifies a Province by Id
      tags:
        - Province
  '/address/country/{countryId}/province/{provinceId}/cities':
    get:
      operationId: get-adress-country-countryId-province-provinceId-cities
      parameters:
        - in: header
          name: applicationId
          required: true
          schema:
            type: string
        - in: header
          name: deviceId
          required: true
          schema:
            type: string
        - description: Id of Country
          in: path
          name: countryId
          required: true
          schema:
            type: string
        - description: Id of Province
          in: path
          name: provinceId
          required: true
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                items:
                  $ref: ../models/Address/City.yaml
                type: array
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: Return all availables Cities for a Province
      summary: Available Cites for Province
      tags:
        - City
  '/address/country/{countryId}/province/{provinceId}/city':
    post:
      operationId: post-address-country-countryId-province-provinceId-cities
      parameters:
        - in: header
          name: applicationId
          required: true
          schema:
            type: string
        - in: header
          name: deviceId
          required: true
          schema:
            type: string
        - description: Id of Country
          in: path
          name: countryId
          required: true
          schema:
            type: string
        - description: Id of Province
          in: path
          name: provinceId
          required: true
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                type: object
                properties:
                  '':
                    $ref: ../models/Address/City.yaml
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: Returns all available Cities from a Province
      summary: Add a City to a Province
      tags:
        - City
  '/address/country/{countryId}/province/{provinceId}/city/{cityId}':
    get:
      operationId: get-adress-country-countryId-province-provinceId-city-cityId
      parameters:
        - in: header
          name: applicationId
          required: true
          schema:
            type: string
        - in: header
          name: deviceId
          required: true
          schema:
            type: string
        - description: Id of Country
          in: path
          name: countryId
          required: true
          schema:
            type: string
        - description: Id of Neighborhood
          in: path
          name: provinceId
          required: true
          schema:
            type: string
        - description: Id of City
          in: path
          name: cityId
          required: true
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: ../models/Address/City.yaml
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: Gets a City by Id
      summary: Get a City by Id
      tags:
        - City
    put:
      summary: Modify a City by Id
      operationId: put-address-country-countryId-province-provinceId-city-cityId
      parameters:
        - in: header
          name: applicationId
          required: true
          schema:
            type: string
        - in: header
          name: deviceId
          required: true
          schema:
            type: string
        - description: Id of Country
          in: path
          name: countryId
          required: true
          schema:
            type: string
        - description: Id of Neighborhood
          in: path
          name: provinceId
          required: true
          schema:
            type: string
        - description: Id of City
          in: path
          name: cityId
          required: true
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: ../models/Address/City.yaml
      description: Modifies a City by Id
      requestBody:
        content:
          application/json:
            schema:
              $ref: ../models/Address/City.yaml
      tags:
        - City
  '/address/country/{countryId}/province/{provinceId}/city/{cityId}/neighborhoods':
    get:
      operationId: get-adress-country-countryId-province-provinceId-city-cityId-neighborhoods
      parameters:
        - in: header
          name: applicationId
          required: true
          schema:
            type: string
        - in: header
          name: deviceId
          required: true
          schema:
            type: string
        - description: Id of Country
          in: path
          name: countryId
          required: true
          schema:
            type: string
        - description: Id of Province
          in: path
          name: provinceId
          required: true
          schema:
            type: string
        - description: Id of City
          in: path
          name: cityId
          required: true
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                items:
                  $ref: ../models/Address/Neighborhood.yaml
                type: array
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: Returns all available Neighborhoods for a City
      summary: Available Neighborhoods for City
      tags:
        - Neighborhood
  '/address/country/{countryId}/province/{provinceId}/city/{cityId}/neighborhood':
    post:
      operationId: post-address-country-countryId-province-provinceId-city-cityId-neighborhood
      parameters:
        - in: header
          name: applicationId
          required: true
          schema:
            type: string
        - in: header
          name: deviceId
          required: true
          schema:
            type: string
        - description: Id of Country
          in: path
          name: countryId
          required: true
          schema:
            type: string
        - description: Id of Province
          in: path
          name: provinceId
          required: true
          schema:
            type: string
        - description: Id of City
          in: path
          name: cityId
          required: true
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                items:
                  $ref: ../models/Address/Neighborhood.yaml
                type: array
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: Returns all available Neighborhoods from a City
      summary: Add a Neighborhood to a City
      requestBody:
        content:
          application/json:
            schema:
              $ref: ../models/Address/Neighborhood.yaml
      tags:
        - Neighborhood
  '/address/country/{countryId}/province/{provinceId}/city/{cityId}/neighborhood/{neighborhoodId}':
    get:
      operationId: get-adress-country-countryId-province-provinceId-city-cityId-neighborhood-neighborhoodId
      parameters:
        - in: header
          name: applicationId
          required: true
          schema:
            type: string
        - in: header
          name: deviceId
          required: true
          schema:
            type: string
        - description: Id of Country
          in: path
          name: countryId
          required: true
          schema:
            type: string
        - description: Id of Province
          in: path
          name: provinceId
          required: true
          schema:
            type: string
        - description: Id of City
          in: path
          name: cityId
          required: true
          schema:
            type: string
        - description: Id of Neighborhood
          in: path
          name: neighborhoodId
          required: true
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: ../models/Address/Neighborhood.yaml
          description: OK
        '401':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unauthorized
        '404':
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: The specified resource was not found
        default:
          content:
            application/json:
              schema:
                $ref: ../models/GvpError.yaml
          description: Unexpected error
      description: Gets a Neighborhood by Id
      summary: Get a Neighborhood by Id
      tags:
        - Neighborhood
    put:
      summary: Modify a Neighborhood by Id
      operationId: put-address-country-countryId-province-provinceId-city-cityId-neighborhood-neighborhoodId
      parameters:
        - in: header
          name: applicationId
          required: true
          schema:
            type: string
        - in: header
          name: deviceId
          required: true
          schema:
            type: string
        - description: Id of Country
          in: path
          name: countryId
          required: true
          schema:
            type: string
        - description: Id of Neighborhood
          in: path
          name: neighborhoodId
          required: true
          schema:
            type: string
        - description: Id of Province
          in: path
          name: provinceId
          required: true
          schema:
            type: string
        - description: Id of City
          in: path
          name: cityId
          required: true
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: ../models/Address/Neighborhood.yaml
      description: Modifies a Neighborhood by Id
      requestBody:
        content:
          application/json:
            schema:
              $ref: ../models/Address/Neighborhood.yaml
      tags:
        - Neighborhood
tags:
  - name: Address
  - name: City
  - name: Country
  - name: Neighborhood
  - name: Province
