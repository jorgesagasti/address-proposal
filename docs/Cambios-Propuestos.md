**Introducción**

**Los objetivos de este proyecto**

Definir las especificaciones funcionales y técnicas de la propuesta de solución para los casos donde Google entrega datos de un Domicilio incompletos o con información no convencional.

**Descripción del problema**

Actualmente se está tomando los datos del Domicilio de un usuario desde un JSON que entrega Google en la mayoría de los casos, la información contenida en dicho JSON es suficiente para contar con los datos mínimos necesarios para un Domicilio, pero existen ciertos casos donde Google NO entrega información completa, por ej, zona rurales, barrios cerrados, etc.

Dicho JSON es tomado por un proceso de BHUB el cual "parsea" dicho JSON e intenta obtener los datos del Domicilio normalizados para luego poder ser utilizados en donde corresponda (por ej: Para el delivery de una Tarjeta Física, el cual no puede ser completado si el Domicilio no es correcto y/o claro).


**Solución Propuesta**

Con el objetivo de lograr de manera conjunta con PPAY una mejor experiencia de Usuario y atacar ese pequeño porcentaje de casos donde no es posible obtener un Domicilio correcto, se propone la siguiente solución para ser evaluada y en caso de ser aceptada proceder al desarrollo de la misma.

Existen varios posibles caminos, algunos con más ventajas y/o requerimiento de esfuerzo que otros. En la siguiente lista se detalla cada uno de ellos siendo el último el presentado por OP con la intención de penalizar lo menos posible la UX del usuario.



* Limitar la funcionalidad de Google de Domicilios para que SOLO acepte domicilios y no cualquier ubicación.
* Parsear u obtener los datos de Domicilio "normalizados de origen" y enviarlos a BHUB ya normalizados. Es decir, que desde el Front se soliciten los datos de Domicilio (Calle, Número, Pais, Provincia, Localidad, etc) y se envíen a BHUB de manera normalizada.
* Parseo x BHUB y posterior validación x PPAY. A continuación se detalla esta última propuesta.

**_Detalle de "Parseo x BHUB y posterior validación x PPAY"_**

Lo que se propone es cambiar el flujo actual donde PPAY obtiene los datos de Google y envía el JSON devuelto por Google a BHUB, y este parsea dicho JSON obteniendo los datos del Domicilio al mejor esfuerzo (dependiendo de lo que Google entregue).

Como mencionamos anteriormente, esto trae aparejado que ciertos casos queden con Domicilio incompleto debido a que Google no entrega dicha información.

La propuesta consta de brindar la posibilidad de incorporar un paso intermedio donde BHUB pueda devolver los datos que pudo obtener del JSON de Google a PPAY, para que PPAY pueda determinar si es necesario solicitarle al usuario que valide y/o modifique dicha información con el objetivo de lograr un Domicilio válido.

En el siguiente diagrama de flujo del proceso propuesto se muestra y luego se detalla los pasos del mismo y las diferentes opciones posibles.



Con los datos del Domicilio parseado por BHUBl, PPAY podría tomar 2 acciones:



1. No analizar la información y devolverla tal cual la recibió de BHUB. En este caso, el resultado sería exactamente igual al actual dejando casos con Domicilios incompletos.
2. Solicitar al usuario que complete los datos faltantes del Domicilio o inclusos todos los datos y luego devolviendo a BHUB los datos cargados por el usuario los cuales conformarán un Domicilio completo.

    Para poder mejorar la experiencia del usuario, PPAY puede obtener de BHUB ciertos datos para pre-cargarlos en listas desplegables (por ej: Provincias, Ciudades, etc) y lograr así una normalización de los datos.  BHUB cuenta con métodos para obtener esta información la cual puede ser generada desde el BackOffice de BHUB o incluso mediante un proceso masivo de carga.


 \
**Alcance de este proyecto**

_Define a nivel Macro el alcance del proyecto en su totalidad._

El alcance de este proyecto consiste en el desarrollo de las funcionalidades necesarias para lograr completar el flujo propuesto.

Se desarrollarán los métodos necesarios para devolver los datos del Domicilio Normalizado luego del Parseo por parte de BHUB como así también para poder recibir los mismos tanto en caso de que PPAY no los modifique como si decide solicitarlos al usuarios.

La solución propuesta será una solución abarcativa la cual puede ser utilizada de manera indistinta según las necesidades del negocio, flujos de la UX del usuario, y/o cualquier otro criterio de decida aplicar PPAY. 

Es decir, por ej en el proceso de Onboarding se podría optar por **NO** requerir al usuario que complete su domicilio tomando el riesgo de que algunos puedan quedar incompletos y en el proceso de Solicitud de Tarjeta Física **SI** requerir al usuario que complete los datos del Domicilio de manera normalizada para asegurarse de que la tarjeta sea entregada de manera correcta.

**NOTA**: Algo importante a tener en cuenta, en caso de decidir avanzar con el desarrollo y la implementación de la solución propuesta, se deberá aplicar la misma en TODOS los puntos del flujo donde se trabaja actualmente con Domicilios (más allá que se opte en algunos puntos por el camino corto, es decir, no solicitarle al usuario los datos)

En caso de avanzar con la solución propuesta, BHUB brindará la información de los nuevos métodos.



**Exclusiones**

_Detalle de las exclusiones del proyecto. Todo lo que se encuentre detallado en esta sección, NO formará parte del alcance del proyecto ni de la responsabilidad de OpenPass._

Este proyecto **NO** considera las siguientes modificaciones y/o adecuaciones:



* Modificación sobre el proceso actual de Parseo.
* Cualquier otro desarrollo y/o cambio que no se encuentre expresamente detallado en este documento.


